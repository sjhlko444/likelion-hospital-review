package com.likelion.likelionhospitalreview.controller;

import com.likelion.likelionhospitalreview.domain.dto.Response;
import com.likelion.likelionhospitalreview.domain.dto.UserDto;
import com.likelion.likelionhospitalreview.domain.dto.UserJoinRequest;
import com.likelion.likelionhospitalreview.domain.dto.UserJoinResponse;
import com.likelion.likelionhospitalreview.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest userJoinRequest) {
        UserDto userDto = userService.join(userJoinRequest);
        return Response.suceess(new UserJoinResponse(userDto.getUserName(),userDto.getEmail()));
    }
}

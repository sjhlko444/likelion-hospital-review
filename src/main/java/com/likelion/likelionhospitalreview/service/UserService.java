package com.likelion.likelionhospitalreview.service;

import com.likelion.likelionhospitalreview.domain.User;
import com.likelion.likelionhospitalreview.domain.dto.UserDto;
import com.likelion.likelionhospitalreview.domain.dto.UserJoinRequest;
import com.likelion.likelionhospitalreview.exception.ErrorCode;
import com.likelion.likelionhospitalreview.exception.HospitalReviewAppException;
import com.likelion.likelionhospitalreview.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    public UserDto join(UserJoinRequest request) {

        //비즈니스 로직 - 회원 가입

        //회원 userName(id) 중복 Check
        //중복이면 회원가입 x --> Exception (예외) 발생
        userRepository.findByUserName(request.getUserName())
                .orElseThrow(() -> new RuntimeException("해당 UserName 중복"));
        // 있으면 에러처리
        userRepository.findByUserName(request.getUserName())
                .ifPresent(user -> {
                    throw new HospitalReviewAppException(ErrorCode.DUPLICATED_USER_NAME, String.format("UserName:%s", request.getUserName()));
                });

        // 회원가입 .save()
        User savedUser = userRepository.save(request.toEntity(encoder.encode(request.getPassword())));
        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .email(savedUser.getEmailAddress())
                .build();
    }
}